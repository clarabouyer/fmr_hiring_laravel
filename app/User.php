<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];




    // Mutator
	public function setPasswordAttribute($value)
	{
		$this->attributes['password']	=	bcrypt($value);
	}



    // Relations
	public function interests()
	{
		return $this->belongsToMany('\App\Models\Interest', 'users_interests', 'user_id', 'interest_id', 'user_id');
	}
}
