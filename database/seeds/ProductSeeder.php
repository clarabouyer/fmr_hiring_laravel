<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class ProductSeeder extends Seeder
{
    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        $json = File::get("database/files/products_descriptions.json");

        $names = json_decode($json);

        $faker = Faker::create();

        foreach ($names as $name => $description) {
            DB::table('products')->insert([
                'name' => $name,
                'price' => $faker->numberBetween(100, 1000),
                'created_at' => date('Y-m-d'),
            ]
        );
    }
}
}
