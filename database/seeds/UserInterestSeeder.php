<?php

use Illuminate\Database\Seeder;

class UsersInterestsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users_interests')->insert([
            'user_id' => 1,
            'interest_id' => 2,
        ]);
        DB::table('users_interests')->insert([
            'user_id' => 1,
            'interest_id' => 3,
        ]);

    }
}
