<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersInterestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::create('users_interests', function(BluePrint $table)
		{
			$table 	->integer('user_id')->unsigned();
			$table 	->integer('interest_id')->unsigned();


			// Relations
			$table	->foreign('user_id')
					->references('id')->on('users');
			$table 	->foreign('interest_id')
					->references('id')->on('interests');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_interests');
    }
}
